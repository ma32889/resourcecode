Spectra tool-kit
================

.. automodule:: resourcecode.spectrum
    :members:

.. automodule:: resourcecode.spectrum.compute_parameters
    :members:
