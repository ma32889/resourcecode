.. Resourcecode documentation master file, created by
   sphinx-quickstart on Thu Jul  1 17:32:05 2021.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

*Resourcecode*, the Marine Data toolbox
=======================================

The aim of RESOURCECODE is to create an integrated marine data toolbox for
developers of ocean energy devices and their supply chain, which will reduce
barriers and promote growth in renewable energy.


.. toctree::
   :maxdepth: 1

   doc_installation
   doc_client
   doc_data
   doc_nataf
   doc_weatherwindow
   doc_produciple_assessment
   doc_opsplanning_resassess
   doc_spectrum


Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`

Acknowledgements
----------------

The ResourceCode Python module was developed by `Logilab <https://logilab.fr/>`_
based on various scientific codes written by the partners of the ResourceCode
Projet: EMEC, CentraleNantes, Ocean Data Lab, Smart Bay Ireland, University
College Dublin, INNOSEA, Ifremer, University of Edinburgh. More information at
http://resourcecode.info.
